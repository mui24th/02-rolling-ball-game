﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class playerJump : MonoBehaviour
{

    public bool onGround;
    public bool falling;
    public bool played;
    public bool played2;
    private Rigidbody rb;
    public GameObject Level1;
    public GameObject player;
    public GameObject quad;
    public AudioSource glassSound;
    public AudioSource fallingSound;
    public AudioSource rollingSound;
    private ParticleSystem puff;
    //static int collisionCount = 0;
    RaycastHit hit;

    void Start()
    {
        onGround = true;
        rb = GetComponent<Rigidbody>();
        puff = player.GetComponent<ParticleSystem>();
        played = false;
        played2 = true;

    }

    //when the player is touching the platform
    void OnCollisionEnter(Collision other)
    {


        if (other.gameObject.CompareTag("ground"))
        {
            onGround = true;
            falling = false;
            puff.Emit(3);
            played = false;
            if (!rollingSound.isPlaying)
            {
                rollingSound.Play();
            }
        }

        if (played == false)
        {
            glassSound.Play();
            played = true;

        }
    }
    //when the player lifts off the plaform not from a jump
    void OnCollisionExit(Collision other)
    {

        if (other.gameObject.CompareTag("ground"))
            { 
                if (rollingSound.isPlaying)
                {
                    rollingSound.Stop();
                }  
            }

    }

    //what is directly under the player
    void FixedUpdate()
    {
        RaycastHit hit;
        Physics.Raycast(transform.position, -Vector3.up, out hit, 100.0f);

        if (hit.collider.gameObject.name == "quad")
        {
            Debug.Log("falling");
            played2 = false;
            if (!fallingSound.isPlaying)
            {
                fallingSound.Play();
            }
            if (rollingSound.isPlaying)
            {
                rollingSound.Stop();
            }
        }
        if (hit.collider.gameObject.tag == "ground")
        {
            Debug.Log("landed");
            if (fallingSound.isPlaying)
            {
                fallingSound.Stop();
            }

            
        }

        if (onGround)
        {
            if (Input.GetKeyDown("space"))
            {
                rb.velocity = new Vector3(0f, 8f, 0f);
                onGround = false;
                falling = true;
                //Debug.Log("in air");
                //play particle system
                played = false;
                if (rollingSound.isPlaying)
                {
                    rollingSound.Stop();
                }
            }

        }
    }


}