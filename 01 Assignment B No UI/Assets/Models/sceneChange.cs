﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneChange : MonoBehaviour 
{
    public GameObject player;
    public string LVL1, LVL2, LVL3, LVL4, LVL5;
    public AudioSource bgm1;
    public AudioSource bgm2;
    public AudioSource bgm3;
    public AudioSource bgm4;
    public AudioSource bgm5;
    Scene currentScene; 
	
	//scene switching when level cleared
    void OnCollisionEnter(Collision collisionObject)
     {
        if (collisionObject.gameObject.name == "player")
        {
            Scene currentScene = SceneManager.GetActiveScene();
            Debug.Log("reached portal");

            if (currentScene.name == "LVL1")
                {
                   SceneManager.LoadScene("LVL2");
                }
            if (currentScene.name == "LVL2")
                {
                    SceneManager.LoadScene("LVL3");

                }
            if (currentScene.name == "LVL3")
                {
                    SceneManager.LoadScene("LVL4");

                }
            if (currentScene.name == "LVL4")
                {
                    SceneManager.LoadScene("LVL5");

                }
            if (currentScene.name == "LVL5")
            {
                SceneManager.LoadScene("LVL1");

            }
        }

    
     }

    //background music

	void Update()
	{
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "LVL1")
            {
            if (!bgm1.isPlaying)
            {
                bgm1.Play();

            }
        }
        if (currentScene.name == "LVL2")
        {
            if (!bgm2.isPlaying)
            {
                bgm2.Play();
              
            }
        }
        if (currentScene.name == "LVL3")
        {
            if (!bgm3.isPlaying)
            {
                bgm3.Play();
             
            }
        }
        if (currentScene.name == "LVL4")
        {
            if (!bgm4.isPlaying)
            {
                bgm4.Play();
             
            }
        }
        if (currentScene.name == "LVL5")
        {
            if (!bgm5.isPlaying)
            {
                bgm5.Play();

            }
        }
	}



}

