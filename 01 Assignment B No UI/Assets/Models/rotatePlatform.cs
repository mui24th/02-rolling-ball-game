﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotatePlatform : MonoBehaviour
{
    void LateUpdate()
    {
        if (Input.GetKey(KeyCode.A))
            transform.Rotate(Vector3.up * 10 * Time.deltaTime, Space.Self);

        if (Input.GetKey(KeyCode.D))
            transform.Rotate(Vector3.down * 10 * Time.deltaTime, Space.Self);

        if (Input.GetKey(KeyCode.W))
            transform.Rotate(Vector3.left * 10 * Time.deltaTime, Space.Self);

        if (Input.GetKey(KeyCode.S))
            transform.Rotate(Vector3.right * 10 * Time.deltaTime, Space.Self);

    }
}


//    //public Time rotationSpeed;
//    public float speed = 0.3f;
//    //the camera
//    public Transform target;
//    public Transform from;
//    public Transform to;

//  void Start()
//  {
//        target = GameObject.Find("CameraControls");
//  }

//  void Update()
//  {
//        float verti = Input.GetAxis("Vertical");
//        float hori = Input.GetAxis("Horizontal");

//        Vector3 relativePos = target.position - transform.position;
//        Quaternion rotation = Quaternion.LookRotation(relativePos);
//        to = Quaternion.Euler(0, 0, 0);
//        from = transform.Rotate((hori - verti) * speed);
//  }

//  //Rotate Platform
//  void FixedUpdate()
//    {


//        //lhs ↪︎ rhs
//        //(euler: rotates around x,y,z axis) rotation lhs/rhs around xyz
//        transform.rotation = Quaternion.Slerp(from ,to,Time.time * speed);


//    }
//}


//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class rotatePlatform : MonoBehaviour
//{
//    public Transform target ;
//    float speed = 0.1f;


//  void Update()
//    {
//        float verti = Input.GetAxis("Vertical");
//        float hori = Input.GetAxis("Horizontal");
//        float step = speed * Time.deltaTime;
//        targetDir = target.position;

//        Vector3 newDir = Vector3.RotateTowards(transform.rotate, targetDir, step, 0.0f);


//        transform.rotation = Quaternion.LookRotation(newDir);
//    }
//}

