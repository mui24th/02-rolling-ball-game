﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class transparency : MonoBehaviour
{
    public GameObject sphere;
    public float alpha = 0.0f;

    void Start()
    {
        sphere = GameObject.Find("Sphere");
        Renderer r = sphere.GetComponent<Renderer>();
        Color Aph = r.material.color;
        Aph.a = alpha;
        r.material.color = Aph;
    }
}
