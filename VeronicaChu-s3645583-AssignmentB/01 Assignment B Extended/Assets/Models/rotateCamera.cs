﻿using UnityEngine;

public class rotateCamera : MonoBehaviour
{
    public Transform player;
    private Vector3 _cameraOffset_x;
    private Vector3 _cameraOffset_y;

    [Range(0.01f, 1.0f)]
    public float camLerp = 0.2f;
    [Range(1.0f, 4.0f)]
    public float rotationSpeed = 1.0f;

    void Start()
    {
        _cameraOffset_x = transform.position - player.position;
        _cameraOffset_y = transform.position - player.position;
        Camera.main.gameObject.AddComponent(typeof(AudioListener));
    }

    //more time between start & decrease shaking when platform rotates
    void LateUpdate()
    {
        //input
        Quaternion camTurnAngle_x = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * rotationSpeed, Vector3.up);
        Quaternion camTurnAngle_y = Quaternion.AngleAxis(Input.GetAxis("Mouse Y") * rotationSpeed, Vector3.left);

        //viewport & snapback 
        _cameraOffset_x = camTurnAngle_x * _cameraOffset_x;
        _cameraOffset_y = camTurnAngle_y * _cameraOffset_y;
        Vector3 newPos = player.position;
        newPos.x = player.position.x + _cameraOffset_x.x;
        newPos.y = player.position.y + _cameraOffset_y.y;
        newPos.z = player.position.z + _cameraOffset_x.z; 
        transform.position = Vector3.Slerp(transform.position, newPos, camLerp);

        transform.LookAt(player.position);
    }
}
