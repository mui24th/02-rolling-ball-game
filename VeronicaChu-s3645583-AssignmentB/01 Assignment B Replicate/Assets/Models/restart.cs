﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class restart : MonoBehaviour
{

    Transform thisTrans;
    Vector3 initialPosition;
    Vector3 initialPosition2;
    Vector3 initialPosition3;
    Vector3 initialPosition4;
    Quaternion initialRotation;
    Quaternion initialRotation2;
    Quaternion initialRotation3;
    Quaternion initialRotation4;
    public GameObject quad;
    public GameObject platform;
    public GameObject platform2;
    public GameObject platform3;
    public GameObject platform4;
    public string LVL1, LVL2, LVL3, LVL4;
    Scene currentScene;


    void Start()
    {
        thisTrans = this.transform;
        //lvl1 initial position
        initialPosition = new Vector3(-5, 6064, -29);
        initialRotation = new Quaternion(0, 0, 0, 0);
        //lvl2 initial position
        initialPosition2 = new Vector3(-2, 31, -7);
        initialRotation2 = new Quaternion(0, 0, 0, 0);
        //lvl3 initial position
        initialPosition3 = new Vector3(-25, 45, -38);
        initialRotation3 = new Quaternion(0, 0, 0, 0);
        //lvl4 initial position
        initialPosition4 = new Vector3(5, 32, -45);
        initialRotation4 = new Quaternion(0, 0, 0, 0);
        //platform identification
        platform = GameObject.Find("Level1");
        platform2 = GameObject.Find("Level2");
        platform3 = GameObject.Find("Level3");
        platform4 = GameObject.Find("Level4");


    }

    //restarting when player falls off plafform
    void OnCollisionEnter(Collision deadPlatform)
    {
      if (deadPlatform.gameObject.name == "quad")
        {
            Debug.Log("this is colliding");
            Scene currentScene = SceneManager.GetActiveScene();
            //Debug.Log("Active scene is '" + currentScene.name + "'.");
            //resets ball position
            if (currentScene.name == "LVL1")
            {
                Debug.Log("this is level1 reset");
                //Debug.Log("dead", gameObject);
                thisTrans.position = initialPosition;
                thisTrans.rotation = initialRotation;
                //reset platform position
                platform.transform.rotation = Quaternion.identity;
            }

            if (currentScene.name == "LVL2")
            {
                Debug.Log("this is level2 reset");
                //Debug.Log("dead", gameObject);
                thisTrans.position = initialPosition2;
                thisTrans.rotation = initialRotation2;
                //reset platform position
                platform2.transform.rotation = Quaternion.identity;
            }

            if (currentScene.name == "LVL3")
            {
                Debug.Log("this is level3 reset");
                //Debug.Log("dead", gameObject);
                thisTrans.position = initialPosition3;
                thisTrans.rotation = initialRotation3;
                //reset platform position
                platform3.transform.rotation = Quaternion.identity;
            }

            if (currentScene.name == "LVL4")
            {
                Debug.Log("this is level4 reset");
                //Debug.Log("dead", gameObject);
                thisTrans.position = initialPosition4;
                thisTrans.rotation = initialRotation4;
                //reset platform position
                platform4.transform.rotation = Quaternion.identity;
            }
  

        }



    }


}