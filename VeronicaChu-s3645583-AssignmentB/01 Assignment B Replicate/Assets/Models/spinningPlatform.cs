﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class spinningPlatform : MonoBehaviour
{
    public GameObject first;
    public GameObject second;
    public GameObject Level3;
    public GameObject plate;
    public GameObject Level4;


    Collider Collider;
    Collider Collider2;
    Collider Collider3;

    Vector3 Center;
    Vector3 Center2;
    Vector3 Center3;

    public string LVL3, LVL4;
    Scene currentScene; 

	// Use this for initialization
	void Start () 
    {
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name =="LVL3")
        {
            first = GameObject.Find("Cylinder_0");
            second = GameObject.Find("Cylinder_(1)_0");
            Level3 = GameObject.Find("Level3");
        }
        else
        {
            plate = GameObject.Find("Pipe"); 
            Level4 = GameObject.Find("Level4");        
        }
     
	}


    //rotating plaforms

	void FixedUpdate()
	{
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "LVL3")
        {
            Collider = first.GetComponent<Collider>();
            Collider2 = second.GetComponent<Collider>();
            Center = Collider.bounds.center;
            Center2 = Collider2.bounds.center;
            first.transform.RotateAround(Center, Level3.transform.forward, 10 * Time.deltaTime);
            second.transform.RotateAround(Center2, -Level3.transform.forward, 10 * Time.deltaTime);
        }
        else
        {
            Collider3 = plate.GetComponent<Collider>();
            Center3 = Collider3.bounds.center;
            plate.transform.RotateAround(Center3, Level4.transform.up, 15 * Time.deltaTime);
        }

    }
}
	
