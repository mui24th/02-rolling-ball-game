﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotatePlatform : MonoBehaviour
{
    void LateUpdate()
    {
        if (Input.GetKey(KeyCode.A))
            transform.Rotate(Vector3.up * 10 * Time.deltaTime, Space.Self);

        if (Input.GetKey(KeyCode.D))
            transform.Rotate(Vector3.down * 10 * Time.deltaTime, Space.Self);

        if (Input.GetKey(KeyCode.W))
            transform.Rotate(Vector3.left * 10 * Time.deltaTime, Space.Self);

        if (Input.GetKey(KeyCode.S))
            transform.Rotate(Vector3.right * 10 * Time.deltaTime, Space.Self);

    }
}
