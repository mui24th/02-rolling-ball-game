﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class playerJump : MonoBehaviour
{

    public bool onGround;
    private Rigidbody rb;
    public GameObject player;
    public GameObject quad;
    private ParticleSystem puff;

    void Start()
    {
        onGround = true;
        rb = GetComponent<Rigidbody>();
        puff = player.GetComponent<ParticleSystem>();
    
    }

    //when the player is touching the platform
    void OnCollisionEnter(Collision other)
    {


        if (other.gameObject.CompareTag("ground"))
        {
            onGround = true;
            puff.Emit(3);
    
        }

    }


    //what is directly under the player
    void FixedUpdate()
    {

     if (onGround)
        {
            if (Input.GetKeyDown("space"))
            {
                rb.velocity = new Vector3(0f, 8f, 0f);
                onGround = false;
                //Debug.Log("in air");
              
            }

        }
    }


}